const PacAndFriends = artifacts.require("PacAndFriends");

module.exports = function (deployer) {
  deployer.deploy(
      PacAndFriends,
      '', // Collection name
      '', // Collection Symbol
      ' ', // BaseURI (can be left blank)
      '' // PAC Sec contract address
  );
};
