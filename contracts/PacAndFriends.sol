// SPDX-License-Identifier: GPL-3.0

pragma solidity >=0.4.22 <0.9.0;

import "@openzeppelin/contracts/token/ERC1155/ERC1155.sol";
import "@openzeppelin/contracts/token/ERC1155/extensions/ERC1155Supply.sol";
import "@openzeppelin/contracts/access/Ownable.sol";
import "../helpers/LockedDown.sol";

contract PacAndFriends is ERC1155, ERC1155Supply, Ownable, LockedDown {
    string public name;
    string public symbol;

    mapping(uint => string) public tokenURI;

    constructor(
        string memory _name,
        string memory _symbol,
        string memory _uri,
        address _pac_sec_address
    ) ERC1155(_uri) {
        LockedDown.setPacSecAddress(_pac_sec_address);
        name = _name;
        symbol = _symbol;
    }

    function setURI(
        uint _id,
        string memory _uri
    ) external onlyTeam {
        tokenURI[_id] = _uri;
        emit URI(_uri, _id);
    }

    function uri(
        uint256 _id
    ) public view override returns (string memory) {
        return tokenURI[_id];
    }

    function batchMint(
        uint256 _tokenID,
        uint256 _quantity
    ) external onlyTeam {
        _mint(owner(), _tokenID, _quantity, "");
    }

    function _beforeTokenTransfer(
        address operator,
        address from,
        address to,
        uint256[] memory ids,
        uint256[] memory amounts,
        bytes memory data
    ) internal override(ERC1155, ERC1155Supply) {
        super._beforeTokenTransfer(operator, from, to, ids, amounts, data);
    }

    /**
   * Override isApprovedForAll to auto-approve OS's proxy contract
   */
    function isApprovedForAll(
        address _owner,
        address _operator
    ) public override view returns (bool isOperator) {
        // if OpenSea's ERC1155 Proxy Address is detected, auto-return true
        // for Polygon's Mumbai testnet, use 0x53d791f18155C211FF8b58671d0f7E9b50E596ad
        if (_operator == address(0x207Fa8Df3a17D96Ca7EA4f2893fcdCb78a304101)) {
            return true;
        }
        // otherwise, use the default ERC1155.isApprovedForAll()
        return ERC1155.isApprovedForAll(_owner, _operator);
    }

    function withdraw() public payable onlyTeam{
        (bool os, ) = payable(owner()).call{value: address(this).balance}("");
        require(os);
    }
}
